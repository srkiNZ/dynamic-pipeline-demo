import os

py_pipeline_template = """
build-job:
    stage: build
    image: python:3.9-slim-buster
    script:
        - echo "building away..."
        - pip install pytest
        - pytest -q
        - python3 src/python/greet.py
"""

def get_srcs():
    src_path = "src"
    srcs = os.listdir(src_path)
    return srcs

def generatePipelines():
    with open("pipelines.yml", "w+") as pf:
        for src in get_srcs():
            if src.startswith("python"):
                pf.write(py_pipeline_template)

if __name__ == "__main__":
    generatePipelines()