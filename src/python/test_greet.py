import greet

def test_greet():
    assert greet.greet("World") == "Hello, World!"
    assert greet.greet("John") != "Hello, World!"
    assert greet.greet("John") == "Hello, John!"